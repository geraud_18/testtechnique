package com.tfj.technologie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechnologieApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechnologieApplication.class, args);
	}

}
