package com.tfj.technologie.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Employee extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long employe_id;

    private String nom;

    private String prenom;

    private String image_url;

    private String email;

    private String login;

    private String mdp;

    private Boolean actif;

    @ManyToOne
    @JoinColumn(name="magasin_id")
    private Magasin magasin_id;

    @OneToMany(mappedBy="directeur_personnel", cascade = CascadeType.ALL)
    private List<Magasin> magasin;
}
