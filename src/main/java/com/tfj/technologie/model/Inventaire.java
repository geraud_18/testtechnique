package com.tfj.technologie.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Inventaire extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long inventaire_id;

    @ManyToOne
    @JoinColumn(name="material_id")
    private Materiel materiel_id;

    @ManyToOne
    @JoinColumn(name="directeur_personnel")
    private Magasin magasin_id;
}
