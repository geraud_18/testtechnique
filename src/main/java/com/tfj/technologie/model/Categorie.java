package com.tfj.technologie.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Categorie extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long categorie_id;

    private String nom;

    @OneToMany(mappedBy="categories", cascade = CascadeType.ALL)
    private List<Materiel> materials;

}
