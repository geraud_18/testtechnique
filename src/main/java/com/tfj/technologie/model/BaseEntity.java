package com.tfj.technologie.model;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;


@Data
@MappedSuperclass
public abstract class BaseEntity {

    @UpdateTimestamp
    private LocalDateTime updated_at;
}
