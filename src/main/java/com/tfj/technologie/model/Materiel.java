package com.tfj.technologie.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Materiel extends BaseEntity{


    enum Taille {PETIT, MOYEN, GRAND};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long materiel_id;

    private String nom;

    private String description;

    private String marque;

    private String image_url;

    private Taille taille;

    private int duree_location;

    private double cout_location;

    private double cout_remplacement;

    @OneToMany(mappedBy="materiel_id", cascade = CascadeType.ALL)
    private List<Inventaire> inventairess;

    @ManyToOne
    @JoinColumn(name="categorie_id")
    private Categorie categories;
}
