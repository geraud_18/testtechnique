package com.tfj.technologie.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Magasin extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long magasin_id;

    private String ville;

    @OneToMany(mappedBy="magasin_id", cascade = CascadeType.ALL)
    private List<Inventaire> inventaire;

    @OneToMany(mappedBy="magasin", cascade = CascadeType.ALL)
    private List<Employee> employee;

    @ManyToOne
    @JoinColumn(name="employe_id")
    private Employee directeur_personnel;

}
