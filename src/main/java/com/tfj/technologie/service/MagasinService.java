package com.tfj.technologie.service;

import com.tfj.technologie.model.Magasin;
import com.tfj.technologie.repository.MagasinRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class MagasinService {

    @Autowired
    private MagasinRepository _magasinRepository;

    public Magasin createMagasin(Magasin magasin){
        Magasin saveMagasin = _magasinRepository.save(magasin);
        return saveMagasin;
    }
}
