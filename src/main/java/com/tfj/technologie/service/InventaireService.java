package com.tfj.technologie.service;

import com.tfj.technologie.model.Inventaire;
import com.tfj.technologie.repository.InventaireRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Data
@Service
public class InventaireService {

    @Autowired
    private InventaireRepository _inventaireRepository;

    public void deleteInventaire(final Long id){
        _inventaireRepository.deleteById(id);
    }

    public Inventaire createInventaire(Inventaire inventaire){
        Inventaire saveInventaire = _inventaireRepository.save(inventaire);
        return saveInventaire;
    }

    public Optional<Inventaire> getInventaire(final Long id){
        return _inventaireRepository.findById(id);
    }

    public Iterable<Inventaire> getInventaires(){
        return _inventaireRepository.findAll();
    }
}
