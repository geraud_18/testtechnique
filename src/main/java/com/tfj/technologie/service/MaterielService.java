package com.tfj.technologie.service;

import com.tfj.technologie.model.Materiel;
import com.tfj.technologie.repository.MaterielRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Data
@Service
public class MaterielService {

    @Autowired
    private MaterielRepository _materielRepository;

    public void deleteMateriel(final Long id){
        _materielRepository.deleteById(id);
    }

    public Materiel createMateriel(Materiel materiel){
        Materiel saveMateriel = _materielRepository.save(materiel);
        return saveMateriel;
    }

    public Optional<Materiel> getMateriel(final Long id){
        return _materielRepository.findById(id);
    }

    public Iterable<Materiel> getMateriels(){
        return _materielRepository.findAll();
    }
}
