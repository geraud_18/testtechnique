package com.tfj.technologie.service;

import com.tfj.technologie.model.Categorie;
import com.tfj.technologie.repository.CategorieRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class CategorieService {

    @Autowired
    private CategorieRepository _categorieRepository;

    public Categorie createCategorie(Categorie categorie){
        Categorie saveCategorie = _categorieRepository.save(categorie);
        return saveCategorie;
    }
}
