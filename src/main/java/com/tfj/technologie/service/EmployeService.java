package com.tfj.technologie.service;

import com.tfj.technologie.model.Employee;
import com.tfj.technologie.repository.EmployeRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class EmployeService {

    @Autowired
    private EmployeRepository _employeRepository;

    public Employee createEmploye(Employee employe){
        Employee saveEmploye = _employeRepository.save(employe);
        return saveEmploye;
    }
}
