package com.tfj.technologie.controller;

import com.tfj.technologie.model.Inventaire;
import com.tfj.technologie.service.InventaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class InventaireController {

    @Autowired
    private InventaireService _inventaireService;

    @GetMapping("/listeInventaire")
    public Iterable<Inventaire> getInventaires(){
        return _inventaireService.getInventaires();
    }

    @PostMapping("/createInventaire")
    public Inventaire createInventaire(@RequestBody Inventaire inventaire){
        return _inventaireService.createInventaire(inventaire);
    }

    @GetMapping("/listeInventaire/{id}")
    public Inventaire getInventaire(@PathVariable("id") final Long id){
        Optional<Inventaire> inventaire = _inventaireService.getInventaire(id);
        if(inventaire.isPresent()){
            return inventaire.get();
        }else{
            return null;
        }
    }

    @DeleteMapping("deleteInventaire/{id}")
    public void deleteInventaire(@PathVariable("id") final Long id){
        _inventaireService.deleteInventaire(id);
    }

    public Inventaire updateInventaire(@PathVariable("id") final Long id, @RequestBody Inventaire inventaire){

        Optional<Inventaire> e = _inventaireService.getInventaire(id);
        if (e.isPresent()){
            Inventaire currentInventaire = e.get();

            return currentInventaire;

        }else{
            return null;
        }
    }
}
