package com.tfj.technologie.controller;

import com.tfj.technologie.model.Employee;
import com.tfj.technologie.service.EmployeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class EmployeController {

    @Autowired
    private EmployeService _employeService;

    @PostMapping("/createEmploye")
    public Employee createEmploye(@RequestBody Employee employe){
        return _employeService.createEmploye(employe);
    }
}
