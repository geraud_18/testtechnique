package com.tfj.technologie.controller;

import com.tfj.technologie.model.Magasin;
import com.tfj.technologie.service.MagasinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class MagasinController {

    @Autowired
    private MagasinService _magasinService;

    @PostMapping("/createMagasin")
    public Magasin createMagasin(@RequestBody Magasin magasin){
        return _magasinService.createMagasin(magasin);
    }
}
