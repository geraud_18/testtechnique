package com.tfj.technologie.controller;

import com.tfj.technologie.model.Categorie;
import com.tfj.technologie.service.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class CategorieController {

    @Autowired
    private CategorieService _categorieService;

    @PostMapping("/createCategorie")
    public Categorie createCategorie(@RequestBody Categorie categorie){
        return _categorieService.createCategorie(categorie);
    }
}
