package com.tfj.technologie.controller;

import com.tfj.technologie.model.Materiel;
import com.tfj.technologie.service.MaterielService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class MaterielController {

    @Autowired
    private MaterielService _materielService;

    @GetMapping("/listeMaterial")
    public Iterable<Materiel> getMateriels(){
        return _materielService.getMateriels();
    }

    @PostMapping("/createMaterial")
    public Materiel createMateriel(@RequestBody Materiel materiel){
        return _materielService.createMateriel(materiel);
    }
}
