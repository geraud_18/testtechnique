package com.tfj.technologie.repository;

import com.tfj.technologie.model.Inventaire;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventaireRepository extends CrudRepository<Inventaire,Long> {
}
