package com.tfj.technologie.repository;

import com.tfj.technologie.model.Materiel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterielRepository extends CrudRepository<Materiel,Long> {
}
