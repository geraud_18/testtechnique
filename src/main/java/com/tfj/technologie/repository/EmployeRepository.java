package com.tfj.technologie.repository;

import com.tfj.technologie.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeRepository extends CrudRepository<Employee,Long> {
}
