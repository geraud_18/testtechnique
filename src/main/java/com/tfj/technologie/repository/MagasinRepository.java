package com.tfj.technologie.repository;

import com.tfj.technologie.model.Magasin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MagasinRepository extends CrudRepository<Magasin,Long> {
}
